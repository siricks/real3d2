<?php

class ModelToolStranica extends Model {

    public function getTotal($data = array()) {
        $sql = 'SELECT COUNT(id) AS total FROM oc_discount_promotions';
        $query = $this->db->query($sql);

        return $query->row['total'];
    }


    public function getAll() {
        $sql = 'SELECT * FROM oc_discount_promotions ORDER BY id DESC';
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function add($promotion ) {
        $sql = "INSERT INTO " . DB_PREFIX . "discount_promotions SET title = '".$promotion['title']."', period = '".$promotion['period']."', description = '".$promotion['description']."'";
        $this->db->query($sql);
        return $this->db->getLastId();
    }

    public function edit($promotion, $id) {
        $sql = "UPDATE " . DB_PREFIX . "discount_promotions SET title = '".$promotion['title']."', period = '".$promotion['period']."', description = '".$promotion['description']."'";

        if(!empty($promotion['picture_1'])) {
            $sql .= ", picture_1 = '".$promotion['picture_1'] . "'";
        }

        if(!empty($promotion['picture_2'])) {
            if(!empty($promotion['picture_1'])) {
                $sql .= ', ';
            }
            $sql .= "picture_2 = '" . $promotion['picture_2'] . "'";
        }

        if(!empty($promotion['picture_3']) && $promotion != false) {
            if(!empty($promotion['picture_2']) || !empty($promotion['picture_1'])) {
                $sql .= ', ';
            }
            $sql .="picture_3 = '".$promotion['picture_3']."'";
        }

        $sql .= " WHERE id = '" . (int)$id . "'";
        $this->db->query($sql);
    }

    public function delete($id) {
        $sql = "DELETE FROM " . DB_PREFIX . "discount_promotions WHERE id = '" . (int)$id . "'";
        $this->db->query($sql);
    }
}