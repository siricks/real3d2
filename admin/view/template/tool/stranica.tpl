<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12 mt-3">
               <h1>Добавить новую акцию</h1>
            </div>
            <form action="<?=$action_add;?>" method="post" enctype="multipart/form-data" >
                <div class="col-xs-6 mt-3">
                    <label>Заголовок</label>
                    <input type="text" class="form-control" name="title" required>
                </div>

                <div class="col-xs-6 mt-3">
                    <label>Период</label>
                    <input type="text" class="form-control" name="period" required>
                </div>

                <div class="col-xs-12 mt-3">
                    <label>Описание</label>
                    <textarea class="form-control" name="description" required></textarea>
                </div>

                <div class="col-xs-4 mt-3">
                    <label>Фон</label>
                    <input type="file" name="picture_1">
                </div>

                <div class="col-xs-4 mt-3">
                    <label>Левый верх</label>
                    <input type="file" name="picture_2" >
                </div>

                <div class="col-xs-4 mt-3">
                    <label>Правый низ</label>
                    <input type="file" name="picture_3">
                </div>

                <div class="col-xs-6 mt-3">
                    <button type="submit" class="btn btn-primary save">Сохранить</button>
                </div>

            </form>
                <hr>
        </div>

    <?php if (isset($promotions)): foreach($promotions as $promotion): ?>
        <div class="row" >
            <form id="form-<?=$promotion['id']?>" action="<?=$action;?>&id=<?=$promotion['id']?>" method="post" enctype="multipart/form-data" >
                <div class="col-xs-6 mt-3">
                    <label>Заголовок</label>
                    <input type="text" class="form-control" value="<?=$promotion['title']?>" name="title" form="form-<?=$promotion['id']?>">
                </div>

                <div class="col-xs-6 mt-3">
                    <label>Период</label>
                    <input type="text" class="form-control" value="<?=$promotion['period']?>" name="period" form="form-<?=$promotion['id']?>">
                </div>

                <div class="col-xs-12 mt-3">
                    <label>Описание</label>
                    <textarea class="form-control" name="description" form="form-<?=$promotion['id']?>"><?=$promotion['description']?></textarea>
                </div>

                <div class="col-xs-4 mt-3">
                    <label>Фон</label>
                    <?php if( !empty($promotion['picture_1']) ):?>
                    <img src="<?=$promotion['picture_1'];?>" class="img-responsive">
                    <?php endif; ?>
                    <input type="file" name="picture_1"  id="picture-1-<?=$promotion['id']?>">
                </div>

                <div class="col-xs-4 mt-3">
                    <label>Левый верх</label>
                    <?php if( !empty($promotion['picture_2']) ):?>
                    <img src="<?=$promotion['picture_2'];?>" class="img-responsive">
                    <?php endif; ?>
                    <input type="file" name="picture_2"  id="picture-2-<?=$promotion['id']?>">
                </div>

                <div class="col-xs-4 mt-3">
                    <label>Правый низ</label>
                    <?php if( !empty($promotion['picture_3']) ):?>
                        <img src="<?=$promotion['picture_3'];?>" class="img-responsive">
                    <?php endif; ?>
                    <input type="file" name="picture_3" id="picture-3-<?=$promotion['id']?>">
                </div>

                <div class="col-xs-6 mt-3">
                    <button type="submit" class="btn btn-primary save" data-id="<?=$promotion['id']?>">Сохранить</button>
                </div>
                <div class="col-xs-6 mt-3">
                    <a href="<?=$delete?>&id=<?=$promotion['id']?>" class="btn btn-danger">Удалить</a>
                </div>

            </form>
        </div>
    <hr>
    <?php endforeach; endif; ?>
    </div>

</div>
<?php echo $footer; ?>