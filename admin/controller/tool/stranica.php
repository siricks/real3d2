<?php

class ControllerToolStranica extends Controller
{
    private $error = array();

    public function index()
    {
        $this->language->load('tool/stranica');

        $this->document->setTitle('Акции');

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('tool/stranica', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->template = 'tool/stranica.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $data = [];

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');


        $this->load->model('tool/stranica');

        $total = $this->model_tool_stranica->getTotal();

        $results = $this->model_tool_stranica->getAll();
        if(isset($results) && is_array($results)) {
            foreach ($results as &$p) {
                if($p['picture_1']) {
                    $p['picture_1'] = '/image/' . $p['picture_1'];
                }

                if($p['picture_2']) {
                    $p['picture_2'] = '/image/' . $p['picture_2'];
                }

                if($p['picture_3']) {
                    $p['picture_3'] = '/image/' . $p['picture_3'];
                }
            }
        }

        $data['promotions'] = $results;

        $data['action'] = $this->url->link('tool/stranica/edit', 'token=' . $this->session->data['token'], true);
        $data['action_add'] = $this->url->link('tool/stranica/add', 'token=' . $this->session->data['token'], true);
        $data['delete'] = $this->url->link('tool/stranica/delete', 'token=' . $this->session->data['token'], true);
        $this->response->setOutput($this->load->view('tool/stranica', $data));
    }

    public function add() {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
            $promotion = $this->request->post;
            $this->load->model('tool/stranica');

            $id = $this->model_tool_stranica->add($promotion);

            if( !empty($_FILES['picture_1']['tmp_name']) ) {
                $promotion['picture_1'] = $this->getFile($_FILES['picture_1'], $id . '-1');
            }

            if( !empty($_FILES['picture_2']['tmp_name'])) {
                $promotion['picture_2'] = $this->getFile($_FILES['picture_2'], $id . '-2');
            }

            if( !empty($_FILES['picture_3']['tmp_name'])) {
                $promotion['picture_3'] = $this->getFile($_FILES['picture_3'], $id . '-3');
            }


            $this->model_tool_stranica->edit($promotion, $id);
        }

        $this->response->redirect($this->url->link('tool/stranica', 'token=' . $this->session->data['token'], true));
    }

    public function edit() {

        if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
            $promotion = $this->request->post;

            if( !empty($_FILES['picture_1']['tmp_name']) ) {
                $promotion['picture_1'] = $this->getFile($_FILES['picture_1'], $this->request->get['id'] . '-1');
            }

            if( !empty($_FILES['picture_2']['tmp_name'])) {
                $promotion['picture_2'] = $this->getFile($_FILES['picture_2'], $this->request->get['id'] . '-2');
            }

            if( !empty($_FILES['picture_3']['tmp_name'])) {
                $promotion['picture_3'] = $this->getFile($_FILES['picture_3'], $this->request->get['id'] . '-3');
            }

            $this->load->model('tool/stranica');
            $this->model_tool_stranica->edit($promotion, $this->request->get['id']);
        }

        $this->response->redirect($this->url->link('tool/stranica', 'token=' . $this->session->data['token'], true));

    }

    public function delete() {
        $id = $this->request->get['id'];
        if(!empty($id)) {
            $this->load->model('tool/stranica');
            $this->model_tool_stranica->delete($id);
        }
        $this->response->redirect($this->url->link('tool/stranica', 'token=' . $this->session->data['token'], true));
    }

    private function getFile($files, $name) {
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $temp = explode(".", $files["name"]);
        $extension = end($temp);
        if ((($files["type"] == "image/gif")
                || ($files["type"] == "image/jpeg")
                || ($files["type"] == "image/jpg")
                || ($files["type"] == "image/pjpeg")
                || ($files["type"] == "image/x-png")
                || ($files["type"] == "image/png"))

        ) {
            if ($files["error"] > 0) {
                $data['error'] = "Error: " . $files["error"] . "<br>";
            } else {
                $data['up'] = "Upload: " . $name . "<br>";
                $data['type'] = "Type: " . $files["type"] . "<br>";
                $data['size'] = "Size: " . ($files["size"] / 1024) . " kB<br>";
                $data['store'] = "Stored in: " . $files["tmp_name"];

                if (file_exists("/image/TEST/" . $name)) {
                    $data['error1'] = $name . " already exists. ";
                } else {
                    move_uploaded_file($files["tmp_name"], DIR_IMAGE . 'promotions/' . $name.$extension);
                   return 'promotions/' . $name . $extension;
                }
            }
        } else {
            $data = false;
        }

        return $data;
    }
}