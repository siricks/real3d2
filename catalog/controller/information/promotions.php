<?php
class ControllerInformationPromotions extends Controller
{
    public function index()
    {
        $this->language->load('information/promotion');

		$this->load->model('extension/promotion');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
            'text' 		=> $this->language->get('text_home'),
            'href' 		=> $this->url->link('common/home')
        );
		$data['breadcrumbs'][] = array(
            'text' 		=> $this->language->get('heading_title'),
            'href' 		=> $this->url->link('information/promotions')
        );

		$url = '';

		if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

		if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

		$filter_data = array(
            'page' 	=> $page,
            'limit' => 7,
            'start' => 7 * ($page - 1),
        );

		$total = $this->model_extension_promotion->getTotal();

		$pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = 7;
		$pagination->url = $this->url->link('information/promotions', 'page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * 7) + 1 : 0, ((($page - 1) * 7) > ($total - 7)) ? $total : ((($page - 1) * 7) + 7), $total, ceil($total / 7));

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_title'] = $this->language->get('text_title');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_date'] = $this->language->get('text_date');
		$data['text_view'] = $this->language->get('text_view');

		$all_news = $this->model_extension_promotion->getAll($filter_data);

        foreach ($all_news as &$p) {
            if($p['picture_1']) {
                $p['picture_1'] = HTTP_SERVER . 'image/' . $p['picture_1'];
            }

            if($p['picture_2']) {
                $p['picture_2'] = HTTP_SERVER . 'image/' . $p['picture_2'];
            }

            if($p['picture_3']) {
                $p['picture_3'] = HTTP_SERVER . 'image/' . $p['picture_3'];
            }
        }


        $data['promotions'] = $all_news;

		$data['logged'] = $this->customer->isLogged();         if(isset($data['logged'])) {             $data['column_left'] = $this->load->controller('common/column_left');         }         else{             $data['column_left'] = '';         }
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('information/promotions', $data));

    }
}