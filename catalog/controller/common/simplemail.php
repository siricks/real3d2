<?php
class ControllerCommonSimpleMail extends Controller
{
    private $error = array();
    public function index()
    {
        $json = 'Произошла ошибка, попробуйте позднее';

        if (!$this->customer->isLogged()) {
            $json = 'Войдите или зарегистрируйтесь, чтобы воспользоваться формой!';
        }


        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->load->model('account/customer');

            $customer_id = $this->customer->getId();

            $customer_info = $this->model_account_customer->getCustomer($customer_id);
            $company_info =  $this->model_account_customer->getCompanyInfo($customer_id);

            $manager = $this->model_account_customer->getManagerById($customer_info['manager_id']);

            $text = '<table style="border-collapse: collapse;">
                         <tbody>
                            <tr><td colspan="2" style="border: 1px solid black; text-align: center">Гарантия лучшей цены</td></tr>
                            <tr><td style="border: 1px solid black;">ФИО: </td><td style="border: 1px solid black;">'.$customer_info['second_name'] . ' ' .$customer_info['firstname'] .' ' . $customer_info['lastname'].'</td></tr>
                            <tr><td style="border: 1px solid black;">Телефон: </td><td style="border: 1px solid black;">'.$customer_info['telephone'].'</td></tr>
                            <tr><td style="border: 1px solid black;">Артикул: </td><td style="border: 1px solid black;">'.$this->request->post['article'].'</td></tr>
                            <tr><td style="border: 1px solid black;">Кол-во: </td><td style="border: 1px solid black;">'.$this->request->post['quality'].'</td></tr>
                            <tr><td style="border: 1px solid black;">Цена: </td><td style="border: 1px solid black;">'.$this->request->post['price'].'</td></tr>
                        </tbody>
                    </table>';

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->hostname = $this->config->get('config_smtp_host');
            $mail->username = $this->config->get('config_smtp_username');
            $mail->password = $this->config->get('config_smtp_password');
            $mail->port = $this->config->get('config_smtp_port');
            $mail->timeout = $this->config->get('config_smtp_timeout');


            $mail->setTo( $manager['email'] );
            $mail->setFrom( $customer_info['email'] );
            $mail->setSender( $customer_info['firstname'] . ' ' . $customer_info['lastname'] );
            $mail->setSubject( strip_tags(html_entity_decode('Гарантия лучшей цены', ENT_QUOTES, 'UTF-8')));
            $mail->setHtml( $text );

            $mail->send();

            $json = 'Спасибо, ваше сообщение отправлено';

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    private function validate() {
        if ((utf8_strlen($this->request->post['article']) < 3)) {
            $this->error['article'] = $this->language->get('error_name');
        }

        if (empty($this->request->post['price']) ) {
            $this->error['price'] = $this->language->get('error_name');
        }

        if ( empty($this->request->post['quality'])) {
            $this->error['quality'] = $this->language->get('error_name');
        }


        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}