<?php
class ControllerCommonSearch extends Controller {
	public function index() {
		$this->load->language('common/search');

		$data['text_search'] = $this->language->get('text_search');

		if (isset($this->request->get['search'])) {
			$data['search'] = $this->request->get['search'];
		} else {
			$data['search'] = '';
		}
        $data['logged'] = $this->customer->isLogged();

		if(isset($data['logged'])) {

            $this->load->model('account/customer');
            $data['customer_info'] = $this->model_account_customer->getCustomer($this->customer->getId());
            if(isset($data['customer_info']['manager_id'])) {
                $data['manager'] = $this->model_account_customer->getManagerById( $data['customer_info']['manager_id'] );

                if(!isset($data['manager']['firstname'])) {
                    $data['manager']['firstname'] = '';
                }

                if(!isset($data['manager']['phone'])) {
                    $data['manager']['phone'] = '';
                }
            }

        }

		return $this->load->view('common/search', $data);
	}
}