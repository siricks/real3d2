<?php

class ControllerAccountQuestion extends Controller
{
    private $error = array();
    public function index()
    {
        $json = 'Произошла ошибка, попробуйте позднее';

        if (!$this->customer->isLogged()) {
            $json = 'Войдите или зарегистрируйтесь, чтобы воспользоваться формой!';
        }


        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->load->model('account/customer');

            $customer_id = $this->customer->getId();
            $customer_info = $this->model_account_customer->getCustomer($customer_id);
            $manager = $this->model_account_customer->getManagerById($customer_info['manager_id']);

            $text = $this->request->post['text'];

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->hostname = $this->config->get('config_smtp_host');
            $mail->username = $this->config->get('config_smtp_username');
            $mail->password = $this->config->get('config_smtp_password');
            $mail->port = $this->config->get('config_smtp_port');
            $mail->timeout = $this->config->get('config_smtp_timeout');


            $mail->setTo( $manager['email'] );
            $mail->setFrom( $customer_info['email'] );
            $mail->setSender(  strip_tags(html_entity_decode($customer_info['firstname'] . ' ' . $customer_info['lastname'], ENT_QUOTES, 'UTF-8')) );
            $mail->setSubject( strip_tags(html_entity_decode('Вопрос клиента', ENT_QUOTES, 'UTF-8')));
            $mail->setText( strip_tags(html_entity_decode($text, ENT_QUOTES, 'UTF-8')) );

            $mail->send();

            $json = 'Спасибо, ваше сообщение отправлено';

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    private function validate() {
        if ( empty($this->request->post['text']) ) {
            $this->error['text'] = $this->language->get('error_name');
        }
        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}