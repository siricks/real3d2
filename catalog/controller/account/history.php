<?php

class ControllerAccountHistory extends Controller
{
    public function index()
    {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/order', '', true);

            $this->response->redirect($this->url->link('account/login', '', true));
        }

        $this->load->language('account/order');

        $this->document->setTitle('История поиска');

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/order', $url, true)
        );

        $data['heading_title'] = $this->language->get('История поиска');

        $data['text_empty'] = $this->language->get('text_empty');

        $data['column_order_id'] = $this->language->get('column_order_id');
        $data['column_customer'] = $this->language->get('column_customer');
        $data['column_product'] = $this->language->get('column_product');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_date_added'] = $this->language->get('column_date_added');

        $data['button_view'] = $this->language->get('button_view');
        $data['button_continue'] = $this->language->get('button_continue');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['orders'] = array();

        $this->load->model('account/order');

        $this->load->model('catalog/product');

        $start = ($page - 1) * 10;
        $finish = $start + 10;
        $data['logged'] = $this->customer->isLogged();
        $order_total = $this->model_catalog_product->getTotal($data['logged'], $start, $finish);//getTotal?

        $results = $this->model_account_order->getOrders(($page - 1) * 10, 10);

        foreach ($results as $result) {
            $product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
            $voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

            $data['orders'][] = array(
                'order_id' => $result['order_id'],
                'name' => $result['firstname'] . ' ' . $result['lastname'],
                'status' => $result['status'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'products' => ($product_total + $voucher_total),
                'total' => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
                'view' => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], true),
            );
        }


        $this->load->language('common/pagination');

        $pagination = new Pagination();

        $pagination->text_first = $this->language->get('text_first');
        $pagination->text_last = $this->language->get('text_last');
        $pagination->text_next = $this->language->get('text_next');
        $pagination->text_prev = $this->language->get('text_prev');

        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = 10;
        $pagination->url = $this->url->link('account/history', 'page={page}', true);




        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($order_total - 10)) ? $order_total : ((($page - 1) * 10) + 10), $order_total, ceil($order_total / 10));

        $data['continue'] = $this->url->link('account/account', '', true);


        if (isset($data['logged'])) {
            $data['column_left'] = $this->load->controller('common/column_left');
        } else {
            $data['column_left'] = '';
        }

        if (isset($data['logged'])) {

            if($page != 1) {
                $start = ($page - 1) * $pagination->limit;
                $finish = $start + 10;
                $data['search_history'] =  $this->model_catalog_product->getProductSearchByUserId($data['logged'], 10, $start, $finish);
            } else {
                $data['search_history'] =  $this->model_catalog_product->getProductSearchByUserId($data['logged'], 10, 0, 10);
            }

            $search_history = $data['search_history'];
            $sort_by_date = [];
            foreach ($search_history as $sh) {
                if(is_array($sh)) {
                    foreach ($sh as $r_key => $row) {
                        $sort_by_date[$row['search_date']][] = '<a href="/index.php?route=product/search&search=' . $row['search_text'] . '">'.$row['search_text'].'</a>';
                    }
                }
            }
            $new_dates = [] ;
            foreach ($sort_by_date as $r_key => $row) {
                $str = implode(" ", $row);
                $new_dates[ $r_key ] = $str;
            }

            $data['search_history'] = $new_dates;

        }

        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('account/history_search', $data));
    }
}