<?php echo $header; ?>
<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) {
             if  ($breadcrumb['text'] == 'Our News') {
                $breadcrumb['text'] = 'Новости';
            }

            ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <div class="row" style="font-family: 'PT Sans', sans-serif !important;"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="col-xs-12">

            <?php foreach ($promotions as $news): ?>

                <div class="row" style="padding-top: 35px; padding-bottom: 44px;  border-top: 2px solid #b2b3b4;">
                    <div class="col-xs-3" style="max-width: 270px; max-height: 200px; padding-left: 0; padding-right: 0">
                        <img src="<?php echo $news['picture_1']; ?>" class="img-responsive" style="box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.2); width: 270px; height: 200px;">
                        <img src="<?php echo $news['picture_2']; ?>" style="width: 80px; height: 80px; position: absolute; top: 0; left: 0">
                        <img src="<?php echo $news['picture_3']; ?>" style="    width: 130px; height: 45px; position: absolute; bottom: 0; right: 0;">
                    </div>

                    <div class="col-xs-9" style="font-size: 16px; padding-left: 40px;">
                        <h2 style="font-size: 18px"><?php echo $news['title']; ?></h2>
                        <p style="font-size: 14px"><?php echo $news['period']; ?></p>
                        <?php echo iconv_substr($news['description'], 0 , 477 ); ?>
                    </div>
                </div>

            <?php endforeach; ?>

            <div class="row" style="border-top: 2px solid #b2b3b4;">
                <div class="col-xs-12 text-center" style="margin-top: 20px"><?php echo $pagination; ?></div>
            </div>

        <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>