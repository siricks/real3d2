<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="h1-after-m-0 "><?php echo $heading_title; ?></h1>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Дата</th>
                                    <th>Запросы</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($search_history)) {
                         foreach ($search_history as $r_key => $sh) {
                            echo '<tr><td>' . date("d.m.Y", strtotime($r_key)) .'</td><td>'. $sh . '</td></tr>';
                         }

                    }?>
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>

            <div class="row">
                <?php if($pagination) {
                       echo $pagination;
                }?>
            </div>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>