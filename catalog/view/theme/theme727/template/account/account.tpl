<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
	<?php if ($success) { ?>
	<div class="alert alert-success">
		<i class="fa fa-check-circle"></i>
		<?php echo $success; ?>
		<button type="button" class="close material-design-close47"></button>
	</div>
	<?php } ?>
	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
		<div class="hystoryblock">
	<h2>Здравствуйте <?php echo $customer_info['lastname'] . ' ' . $customer_info['firstname'] . ' ' . $customer_info['secondname']; ?></h2>
	<div class="persblock">
		<div class="txthd">
			<img src="man.png">Ваш персональный менеджер
		</div>
		<div class="userinfo">
			<div class="avatar"><img src="noavatar.jpg"></div>
			<div class="datas">
				<div class="row">
					<div class="rowname">ФИО:</div>
					<div class="rowvalue"><?php echo $current_manager['lastname'] .' '. $current_manager['firstname']; ?></div>
				</div>
				<div class="row">
					<div class="rowname">E-mail:</div>
					<div class="rowvalue"><?php echo $current_manager['email']; ?></div>
				</div>
				<div class="row">
					<div class="rowname">Телефон:</div>
					<div class="rowvalue"><?php echo $current_manager['phone']; ?></div>
				</div>
				<div class="row">
					<div class="rowname">ICQ:</div>
					<div class="rowvalue"><?php echo $current_manager['icq']; ?></div>
				</div>
			</div>
		</div>
		<div class="sendblock">
			<a class="sendbut" href="#" data-toggle="modal" data-target="#question" >Отправить сообщение</a>
		</div>
	</div>
	<div class="platblock">
		<div class="txthd">
			<img src="historyplat.png">История платежей и кредиты
		</div>
		<div class="histinfo">
			<input class="tabradio" type="radio" id="tab1" name="tabselect" checked><label for="tab1" class="trlabel">Кредиты</label>
			<input class="tabradio" type="radio" id="tab2" name="tabselect"><label for="tab2" class="trlabel">Платежи</label>
			<div class="hitable">
				<table>
					<tr>
						<td>Кредит</td><td>Дни</td><td>Текущая задолженность</td><td>Просроченный долг</td><td>Доступный лимит</td>
					</tr>
					<tr>
						<td>02.03.2018</td>
					</tr>
					<tr>
						<td>20 000 <span>рублей</span></td>
						<td>60 <span>дней</span></td>
						<td>15 000 <span>рублей</span></td>
						<td>0.00 <span>рублей</span></td>
						<td>10 000 <span>рублей</span></td>
					</tr>
					<tr>
						<td>02.03.2018</td>
					</tr>
					<tr>
						<td>20 000 <span>рублей</span></td>
						<td>60 <span>дней</span></td>
						<td>15 000 <span>рублей</span></td>
						<td>0.00 <span>рублей</span></td>
						<td>10 000 <span>рублей</span></td>
					</tr>
				</table>
			</div>
			<div class="hitable">
				<table>
					<tr>
						<td>Дата/Время</td><td>Реквизиты</td><td>Описание</td><td>Сумма</td><td>Статус</td>
					</tr>
					<tr>
						<td>02.03.2018</td>
					</tr>
					<tr>
						<td>13:45</td>
						<td class="tcol2">Карта Универсальная GOLD * 4843</td>
						<td class="tcol3">Оплата заказаза № 1265749</td>
						<td>30 000 <span>рублей</span></td>
						<td>Оплачено</span></td>
					</tr>
					<tr>
						<td>02.03.2018</td>
					</tr>
					<tr>
						<td>13:45</td>
						<td class="tcol2">Карта Универсальная GOLD * 4843</td>
						<td class="tcol3">Оплата заказаза № 1265749</td>
						<td>30 000 <span>рублей</span></td>
						<td>Оплачено</span></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>
<?php echo $footer; ?>

<!-- Вопрос Менеджеру-->
<div class="modal fade" id="question" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close best_price_warranty_button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<div class="row">
					<div class="col-sm-12" style="margin-bottom: 2rem">
						<form name="question_form" id="question_form">
							<p style="font-size: 14px;color: #2f84a1;font-weight: bold; text-shadow: 5px 3px 10px rgba(0,0,0,0.5);">Ваш вопрос или сообщение для вашего менеджера:</p>
							<textarea name="text" class="form-control" rows="5" required></textarea>
							<p id="status_question_form" class="pull-left mt-10"></p> <button type="submit" class="btn btn-primary pull-right mt-10">Отправить сообщение</button>
						</form>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- ./Вопрос Менеджеру- -->

<script>
    $(document).ready(function () {
        $('#question_form').submit(function (event) {
            event.preventDefault();

            $.ajax({
                url: 'index.php?route=account/question',
                type: 'post',
                data: $(this).serialize(),
                dataType: 'json',
                complete: function () {

                },
                success: function (html) {
                    console.log(html);
                    $('#status_question_form').text(html);
                },
                error: function (xhr, ajaxOptions, thrownError) {

                }
            });
        });
    });

</script>
