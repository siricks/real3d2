<?php echo $header; ?>

<?php
		$showtop = '';
		$showbot = '';
		$dontshow = '';

		 if ($column_left && empty($products)){
			$showtop = true;
		} elseif ($column_left && !empty($products)){
			 $showbot = true;
		}
		else{
			$dontshow = true;
		}
	?>

<div class="container">
    <!--<ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>-->
    <div>
        <?php if ($thumb || $description) { ?>
        <div class="row">
            <?php if ($thumb) { ?>
            <div class="col-sm-3">
                <span class="image-category"><img width="<?php echo $thumb_width; ?>" height="<?php echo $thumb_height; ?>" src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></span>
            </div>
            <?php } ?>
            <?php if ($description) { ?>
            <div class="description description-category <?php echo $thumb ? 'col-sm-9' : 'col-sm-12'; ?>">
                <h2><?php echo $heading_title; ?></h2>
                <?php echo $description; ?>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
        <?php  if ($categories) { ?>
        <?php  if($showbot || $dontshow){ echo '<h3>' . $text_refine .'</h3>' ;}?>





        <div class="row">
            <?php if ($showtop){ echo $column_left; } ?>
            <div class="col-sm-12 <?php if ($showtop) {  echo 'col-md-9' ; }?>" >
                <?php if ($showtop) { ?>
                <h3> <?php echo $text_refine; ?> </h3> <?php } ?>

                <ul class="box-subcat">
                    <?php foreach ($categories as $category) { ?>
                    <li class="col-sm-2">
                        <div class="thumb">
                            <?php if ($category['thumb']) { ?>
                            <div class="image"><a href="<?php echo $category['href']; ?>"><img width="<?php echo $thumb_width; ?>" height="<?php echo $thumb_height; ?>" src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" /></a></div>
                            <?php } ?>
                            <div class="name subcatname"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>

        </div>
        <?php } ?>
    </div>

    <div class="row"><?php  if ($showbot){ echo $column_left; } ?>
        <?php if ($showbot && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($showbot || $column_right) { ?>
        <?php $class = 'col-sm-12 col-md-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <style>
            @media (min-width: 992px) {
                .category-table {
                    width: 78%;
                }
            }

        </style>
        <div id="content" class="<?php echo $class; ?> category-table"><?php echo $content_top; ?>
            <?php { ?>
            <?php if(count($products) > 0) { ?>
            <div class="product-filter clearfix">
                <!--<div class="product-filter_elem">
                    <div class="button-view">
                        <button type="button" id="grid-view" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="material-icons-view_module"></i></button>
                        <button type="button" id="list-view" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="material-icons-view_list"></i></button>
                    </div>
                </div>-->
                <div class="product-filter_elem sort">
                    <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                    <select id="input-sort" onchange="location = this.value;">
                        <?php foreach ($sorts as $sorts) { ?>
                        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="product-filter_elem show pull-right">
                    <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                    <select id="input-limit" onchange="location = this.value;">
                        <?php foreach ($limits as $limits) { ?>
                        <?php if ($limits['value'] == $limit) { ?>
                        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <!--<div class="product-filter_elem">
                    <div class="button-view">
                        <a href="<?php echo $compare; ?>" id="compare-total" class="compare-total material-icons-repeat" data-toggle="tooltip" title="<?php echo $text_compare; ?>"><span><?php echo $text_compare; ?></span>
                        </a>
                    </div>
                </div>-->
            </div>
            <style>
                .price {
                    color: #0082B1!important;
                }
            </style>



            <div class="table-responsive">
                <table class="table table-striped <?php
                if ( !isset($logged) ) {
                        echo ' tabled-unlogged';
                }?>">
                    <thead>
                    <tr>
                        <th>Фото</th>
                        <th>Наименование</th>
                        <th>Артикул</th>
                        <th>Бренд</th>
                        <th>Цена</th>
                        <th>Кол</th>
                        <th>Резерв</th>
                        <th colspan="3">
                            <button class="btn btn-primary" data-toggle="modal" data-target="#garantiaBestPrice">Гарантия лучшей цены</button>
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $pr = 10000; $clr = 0; foreach ($products as $product) { $pr++; $clr++; ?>
                    <tr>
                        <td>
                            <div class="image">

                                <?php if ($product['additional_thumb']) { ?>
                                <a class="lazy"  data-fancybox="gallery-<?php echo $product['product_id'];?>" href="<?php echo $product['full_img']; ?>" style="background:none; padding-bottom:50px">
                                    <i class="fa fa-picture-o fa-3x" aria-hidden="true"></i>
                                    <img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" style="display: none" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-secondary" data-src="<?php echo $product['additional_thumb']; ?>" src="#"/>
                                    <img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" style="display: none" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-primary" data-src="<?php echo $product['thumb']; ?>" src="#"/>
                                    <?php } else { ?>
                                    <a class="lazy" data-fancybox="gallery-<?php echo $product['product_id'];?>" href="<?php echo $product['thumb']; ?>" style="background:none; padding-bottom:50px">
                                        <i class="fa fa-picture-o fa-3x" aria-hidden="true"></i>
                                        <img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" style="display: none" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img" data-src="<?php echo $product['thumb']; ?>" src="#"/>
                                        <?php } ?>
                                    </a>
                                    <?php if ($product['images']) {
                                        foreach ($product['images'] as $img) {
                                            echo '<a href="'.$img['popup'].'" data-fancybox="gallery-'.$product['product_id'].'" style="display: none;"></a>';
                                }

                                ?>

                                <?php  }?>

                                <?php if ($product['rating']) { ?>
                                <div class="rating">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                    <?php if ($product['rating'] < $i) { ?>
                                    <span class="fa-stack"><i class="material-icons-star fa-stack-1x"></i></span>
                                    <?php } else { ?>
                                    <span class="fa-stack"><i class="material-icons-star star fa-stack-1x"></i></span>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                        </td>
                        <td>
                            <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                        </td>
                        <td>
                            <?php if ($product['model']) { ?>
                            <div class="price price-product">
                                <?php echo $product['model']; ?>
                            </div>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if ($product['manufacturer']) { ?>
                            <div class="price price-product">
                                <?php echo $product['manufacturer']; ?>
                            </div>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if ($product['price']) { ?>
                            <div class="price price-product">
                                <?php if (!$product['special']) { ?>
                                <?php echo $product['price']; ?>
                                <?php } else { ?>
                                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                <?php } ?>
                                <?php if ($product['tax']) { ?>
                                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if ( isset($product['quantity']) && $product['quantity']> 0) { ?>
                            <div class="price price-product" id="product_quantity_label_<?php echo $product['product_id'] ?>">
                                <?php
                                if ( isset($product['reserve'])) {
                                        $max = $product['quantity'] - $product['reserve'];
                                } else {
                                        $max = $product['quantity'];
                                }

                                if ($max < 0 ) {
                                    $max = 0;
                                }

                                echo $max;
                                    ?>
                                  <?php } ?>
                        </td>
                        <td>
                            <div class="price price-product" id="product_reserve_<?php echo $product['product_id'] ?>">
                            <?php if ( isset($product['reserve'])) {
                                        echo $product['reserve']; ?>

                            <?php } ?>
                            </div>
                        </td>
                        <td>
                            <?php if (isset($product['quantity']) && $product['quantity']> 0) { ?>

                            <input type="number" style="width: 50px; padding-left: 10px; padding-right: 0;" id="product_quantity_custom_<?php echo $product['product_id'] ?>"
                                   class="product_quantity" data-max="<?php echo $max; ?>"
                                   value="1">

                            <?php } ?>
                        </td>

                        <td>
                            <?php if (isset($product['quantity']) && $product['quantity']> 0) { ?>
                            <button class="btn btn-primary <?if ($max == 0) {echo 'disabled'; }?>" id="bye_button_<?php echo $product['product_id'];?>"  style="background: #0082B1!important;" type="button" <?php if (count($product['options']) > 3) { ?> onclick="cart.add('<?php echo $product['product_id']; ?>');" <?php } else { ?> onclick="
                            if( !$(this).hasClass('disabled')) {
                                ajaxAdd($(this),<?php echo $product['product_id'] ?>, $('#product_quantity_custom_<?php echo $product['product_id'] ?>').val() );
                            }
                            //$(this).addClass('disabled');" <?php } ?>><?php echo $button_cart; ?></button></td>
                        <?php } else { ?>
                            <button class="btn btn-primary disabled"  style="background: #0082B1!important;" type="button">
                                <?php echo $button_cart; ?>
                            </button>
                        <?php } ?>
                        <style>
                            .star-cl {
                                width: 25px;
                                height: 25px;
                                background-position: center center;
                                background-repeat: no-repeat;
                                background-size: 100%;
                                background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAhCAYAAABTERJSAAADbklEQVRYhcXXa2yeYxgH8F/Xg0MtMx1iH2hpt5ljmOMXDSL2gVAmjhPJWEIkW6jiy4aEYBISKSEZIopkyBrsC+GDEBZh6RyytQ5bs2abU8QmTVfz4brf9Nm7Pk/tfa39J3fu633u0/++3uv+39dd45F3VYlrsQr3441qJppWJZEGrMAJWInDppLMIpya7Lm4earI1OO+ZL+f6k4cPhVkOnA6+nATPkcbbplsMvUiYOFp/IEn0+9OHDGZZK7Gmfje2AnqxXqchFsni0w9upL9FHYnewSPJnsZGieDzJU4C5vQU9bWiy/QisWTQeaBVGe9UsI/eDzZ9+DQA5m4rqDtEMzGsWgWJ2UBzsaA/b1SwloRO+dgHb4UXvwZ27AdO/LINGK+UNFWEYAtqRxnf1UdFbHxVw6ZUTyENWhPJYtfMZjI/YTNye6rw4c4FzVlg0YwlAb8iB/SDgfSzovwXpqzJW2wObPB2TgjlSy21GFnhkivOKrbsBW/4M8JFs5DXypZ1OIYzBJe78DS1La+DtfjFVyDefgWGyokMBFGhbeHkt2evq/B4mnYJeS8B3PwMS45SGRKuDCtMxeviwv279LRHha6sBpH4h1ccZCIXCrC4Wi8LO6yYfbVmVEswbOYLmLnuv+ZyGV4G014EbendZWTgb24G0+IVKBHlTlKBh2JyHQ8gzuwJ9shT4G7hFbUiuBeUiWRRWJjjUK5l43Xqeg6WIkHhcS/gAsqJHIiXhWK/jDuzes40d30GDYKHSq6OoqwV3h4h7FbvSIyM4Ri7hbKWwkGhXLPEke5YjJtQjEHhFJXghH0p7VaqyHTkur+NGml+CrVp1RDpvQM+bqgTwPOx8yCPt+len41ZOakevM4bTVCOz7BZ+J1sNT46eamDJncg1BEphYnl01WwuX4AG+JJGpIxNfzidRt9s3yBvG7yJmaKiHThONFCtGfvrWLB9s6XCwC+y6x46vwqYiL1cJjNwhP7BTenSGStwMm0yzi4Jtkv4mPsFDkO10igeoW76a1uEhcHxtEetqTSC00Fje5x7tIyNpSPU9k/A0i2epOZfs4Y/bgNZGf3IjlOE9487fU57S8BYs8syDVM8UVv0rsdkUOkSyG8ZK4Qu4Uf+dRqa083fxPZLaKv6NbvJM6sWUCEuXYhefS+OUii9yY1/lfLd274cZsgSsAAAAASUVORK5CYII=');
                            }

                            .star-cl:hover{
                                background-position: center center;
                                background-repeat: no-repeat;
                                background-size: 100%;
                                background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAhCAYAAABTERJSAAACGklEQVRYhcXXP2gUQRTH8c8dAdFGEMyBiv9N/B/wTyliYRUiChaKNlGbaCEEREnhv4DBoCgWJoVIGkELQUGsxVKtxEoDKgoiioigKKSwmD2I6+3uZXdz94PhZue9ffO9uZk37yqGHymoPtzCcdwvEqhalARj6MTNooGKwuzH4qjfiUPthBmNPV8tEqwIzB6siI3V0N8OmOsJ4yN5A+aF6fP/qtRVw7FWwmTtjUt5guaB6cWaDJ+FONIKmKS9EteMV6cjw17DUqzGWvRE/WZUw1O8wCTe4D0+4lcSTAVbsSpqK4XNuQxLMKfJyRtpR9Sm6xs+RGBv8S76fN4RkS4qMOFMtSBqPXFDFZ9aCJKmJ1Vsw8M2gzzArvpp2ou7bQK5h338e7QPYqLFIBM4UH+I55l+JdQlTeq22KXaKOmdwJVZBrmBo/HBpAx8CsOzBHINJxsZ0q6DsxjC75Ig/uA8BpMcsu6mEbwsCeY7LqQ5ZMFUsLwkmJpw3eSG2SQU2mVpS5oxC2ZDiSCwLs2YBdNVIggFYbpLBMmMlwWzsclJvjTp1425eWDmy1hWoUAaEE5cr1DVpWkeNueB6ZJcln7FaaEEHRfKyMfYjsN4lRI38VCkwTT6fX/inJAvRjHVwOeOkBL68bqBfX0emJ3T+lO4HEFcxI+U9+qaEL7QgFDj1rU7D8yksDHHhP9JZ/C5CYi4xqP3B6OYz5Ic/wKZDE2+/f9lwQAAAABJRU5ErkJggg==');
                            }

                            .in_cart {
                                display: none;
                            }

                            .cart_img {
                                max-height: 25px;
                            }

                        </style>

                        <td>
                            <div class="row">
                                <div class="col-xs-6">
                                    <button class="product-btn star-cl" type="button" data-toggle="tooltip" data-product-id="<?php echo $product['product_id']; ?>"
                                            title="<?php echo $button_wishlist; ?>"
                                            onclick="wishlist.add($(this).data('product-id'))">
                                        <span> </span>
                                    </button>
                                </div>

                                <div class="col-xs-6">
                                    <?php
                                        if(isset($cartData)) {
                                            if( array_key_exists($product['product_id'], $cartData) ) { ?>
                                    <img src="/shopping.png" class="image-responsive cart_img" data-in-cart="<?php echo $product['product_id']; ?>">
                                    <?php } else { ?>
                                    <img src="/shopping.png" class="image-responsive cart_img in_cart"  data-in-cart="<?php echo $product['product_id']; ?>">
                                    <?php } ?>
                                    <?php } else { ?>
                                    <img src="/shopping.png" class="image-responsive cart_img in_cart" data-in-cart="<?php echo $product['product_id']; ?>">
                                    <?php }  ?>
                                </div>
                            </div>
                        </td>

                    </tr>
                    <?php };?>
                    </tbody>
                </table>
                <!--<pre>
                    <?php //var_dump($products);?>
                </pre>-->
            </div>

        </div>
        <?php } ?>
        <?php if($pagination) { ?>
        <div id="product-preloader">
            <span class="preloader"></span>
            <button class='load-more btn'><?php echo $text_load_more; ?></button>
        </div>
        <div class="pagination-block text-center"><?php echo $pagination; ?></div>
        <?php } ?>
        <?php } ?>
        <?php if (!$categories && !$products) { ?>
        <p><?php echo $text_empty; ?></p>
        <div class="buttons">
            <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
        </div>
        <?php } ?>
        <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
</div>



</div>
<?php echo $footer; ?>


<!-- Гарантия лучшей цены -->
<div class="modal fade" id="garantiaBestPrice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close best_price_warranty_button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="row">
                    <div class="col-sm-12" style="margin-bottom: 2rem">
                        <img class="img-fluid" src="/luchsh_tcsena.jpg">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Артикул товара" style="margin-bottom: 1rem" form="garant_form" name="article" required>
                        <input type="text" placeholder="Ваша цена" form="garant_form" name="price" required>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Кол-во" style="margin-bottom: 1rem" form="garant_form" name="quality" required>
                        <button class="btn btn-primary btn-lg" style="width: 100%;height: 40px;" form="garant_form" type="submit">Отправить</button>
                    </div>
                    <form name="garant_form" id="garant_form"></form>
                    <div class="col-sm-12 mt-10">
                        <p id="status_form"></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- ./Гарантия лучшей цены -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

<script>
    /**
     * Функция не дает сделать меньше 1 и больше max
     * @param obj
     * @param max
     */
    $('.product_quantity').on('input', function() {

        var max = $(this).data('max');
        var value = $(this).val();


        if( value > max) {
            $(this).val(max);
        }
        if( value < 1) {
            $(this).val(1);
        }
    });

    $(document).ready(function () {
       $('#garant_form').submit(function (event) {
           event.preventDefault();

           $.ajax({
               url: 'index.php?route=common/simplemail',
               type: 'post',
               data: $( this ).serialize(),
               dataType: 'json',
               complete: function() {

               },
               success: function(html) {
                    console.log(html);
                    $('#status_form').text(html);
               },
               error: function(xhr, ajaxOptions, thrownError) {

               }
           });
       });
    });

</script>

