<style>
	.stat-footer ul{
		list-style: none;
		margin-left: 0;
		padding-left: 0;
		font-family: 'PT Sans Caption', sans-serif !important;
	}
    .stat-footer ul li,
    .stat-footer ul li a,
    .stat-footer ul li a:hover,
    .stat-footer ul li a:active {
        color: white!important; text-decoration: unset; font-size: 15px; font-weight: lighter;
    }
</style>
<footer style="background-color: #021924; color: white;">
	<div class="container stat-footer" style="display: block; padding-bottom: 28px;">
		<div class="row">
			<div class="col-xs-3">
				<ul>
					<li><a href="/index.php?route=product/category&path=55">Доставка и оплата</a></li>
					<li><a href="#">Условия возврата и гарантии</a></li>
					<li><a href="#">Пользовательское соглашение</a></li>
					<li><a href="/index.php?route=product/category&path=42">Помощь</a></li>
				</ul>
			</div>

			<div class="col-xs-2">
				<ul>
                    <li><a href="/index.php?route=product/category&path=18">Каталог товаров</a></li>
                    <li><a href="/index.php?route=product/category&path=28">Акции</a></li>
                    <li><a href="#">Бренды</a></li>
                    <li><a href="/index.php?route=information/news">Новости</a></li>
                </ul>
			</div>

			<div class="col-xs-4">
				<ul>
                    <li><a href="/index.php?route=information/contact">Контакты:</a></li>

					<li><svg  style="margin-right: 15px"
								xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink"
								width="13px" height="21px">
							<image  x="0px" y="0px" width="13px" height="21px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAVCAMAAACqsJS4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABFFBMVEX9/f78/f78/P38/f79/f78/f78/f78/f79/f79/f79/f78/f78/f78/f78/f79/f78/f78/f78/f78/f78/f79/f78/f78/f78/f78/f78/f79/f78/f78/P38/f78/f78/f78/f79/f78/f78/f78/f78/f79/f78/f78/f78/f78/f79/f78/f79/f78/f78/f78/f78/f78/f78/f78/f78/f78/f78/f38/f78/f78/f78/f79/f78/f78/f78/P38/f78/f79/f78/f78/f78/f78/f78/f79/f78/f78/f79/f78/f78/f78/f39/f79/f78/f78/f78/f78/f78/f78/f78/f78/P39/f7///9/WZObAAAAWHRSTlMAAAAngsXagCYBX+jnXQFZ+vlWIeS1k7bjH3lzERJ1dL+mqrxXW9jcUpugxHv0YQQFZHcm6feif6P4JAKXQ+/uPgO3tFj9VQjU0G1qG+IaiYY16+ozcddvsvZv4gAAAAFiS0dEW3S8lTQAAAAHdElNRQfiBAEXDzv1npGSAAAA20lEQVQY0yWO11pCMRCEZ6PnWFAEVMSKKHax92NDBOy9TPb9H8RNnORiJpP99gcEPb1JmvT1iwhkYDDHoKHhvMCNFLx5r1osCUbHvHK8PGEvlUlMkcXpGczOFcgq5pW1OL9Av4hUtW5eZIlcxgq5KqGskykScg2hWyc3sKna2NqWnd0GuYf9A+rh0fHJKZk7Q3ZOqh1TOYNcXBpJuFfXRta84b9azbDpth3Ldkecpe5dHLvvStz0EP75x4Bg8ali6fkl0kLyr5be4GLn8P7hP78cEOkl+/75zYL5AzPvMO2dimH/AAAAAElFTkSuQmCC" />
						</svg><a href="/index.php?route=information/contact">Санкт-Петербург, ул Предпортовая 6</a></li>
					<li><svg    style="margin-right: 15px"
								xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink"
								width="17px" height="16px">
							<image  x="0px" y="0px" width="17px" height="16px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAQCAMAAADH72RtAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAA81BMVEX9/f78/P38/f78/f38/f78/f78/P38/f79/f79/f78/f78/P38/f78/f79/f78/P38/f78/f78/f78/P38/f79/f78/f78/f79/f78/f79/f78/f79/f78/f78/f79/f78/f79/f78/f78/f78/f78/f78/f78/P38/f79/f79/f78/f78/f78/f78/f78/f78/f78/f38/f38/f79/f78/f79/f78/f78/f79/f78/f78/f78/f78/P38/f78/f79/f79/f78/P38/f78/f78/f78/f78/f78/f78/f78/f78/f78/f78/P39/f78/f3///+X7zrsAAAATHRSTlMAAAAACGXdpxeJowlc823KwgLwf+jiag27+5Bf/g/I7WNA8kkLDAFo+eNWBkKdoVd/+4o/a+KMEgR89ues4+M3o/rCOgpBlM3hw3IckxqcXwAAAAFiS0dEUONuTLwAAAAHdElNRQfiBAEXEBJ6dgdgAAAAyUlEQVQY0y2P6VbCQAyF01itiAgqVQoopbgBihtLAS2boGBm+v5vQ9KZ/Mg595ube+4AuIdH3rGDPLwOEMDNkaKTPDDIMMIp8RTO0Ch+gKIQKp0bwja4YK3p0hCOAihrrfyraxsivgrpoGoSLKkR1T17IRrh5pZUI0SrQIKakSa/lXnu7rPzh8cnStudLuLzS+/VRQl8e9cUfXz2B0TRcCS38XiiUpoG3EzpL/E4cfgtPbWsxPaYzRfZb2i5AlMEftab3+Rvu/vHPeiLHiZG3c3KAAAAAElFTkSuQmCC" />
						</svg><a href="tel:88123846916">8 812 384 69 16 </a></li>
					<li><svg    style="margin-right: 15px"
								xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink"
								width="18px" height="15px">
							<image  x="0px" y="0px" width="18px" height="15px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAPCAMAAADeWG8gAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABUFBMVEX8/f79/f78/P38/f78/f79/f78/f79/f78/f79/f78/f78/f78/f79/f78/f79/f78/f78/f79/f78/f78/f79/f78/f79/f78/f78/f79/f79/f79/f78/f79/f78/f38/f78/P38/f78/P38/f78/f79/f79/f78/f79/f78/f78/f78/f78/f78/f78/f78/f78/f78/f78/P39/f78/f78/P38/f79/f78/f78/f78/f79/f78/f78/f78/f79/f78/f78/f78/f78/f78/f78/P39/f78/f78/f39/f78/P38/f79/f78/f78/f78/f79/f78/f78/f78/f79/f78/f78/f78/f78/P38/f78/f78/f78/f78/f78/f78/f78/f78/f78/P38/f78/f78/f78/P39/f78/f78/f38/f78/f39/f78/P3///+qtUpaAAAAa3RSTlMAAAAEdOHhdAM0w8EzCYD19X8IPtLQPZL+14OAfnV2ftb9kQIwgj0vMS48LuhnQyARc3dxDiJFZvKZDhBqbWgNmu3QNHZ5ee1iCAwMCP6WB5f0ychSUviexPOspvf5qaj889+lwqLbRMr0RT21JPgAAAABYktHRG9VCGGBAAAAB3RJTUUH4gQBFxAgsqFW4AAAAO9JREFUGNMtj+dTwkAQxV9iw0awoIhGbCgqKmBDFOFADSoWwN57e+b+/4/eJb7Z2Zn9zZa3gKem5pbWtgD+ZRhAewfJ384uVfmwO2jRVYyhnl6f9PXTJyqHBwwTgcHIUHR4xLbt0VgwOjY+MYmpOKcBcyaRmFWb5hifR3JhcQlIpTOZ5RVgdW09iSw3cgY2t/L57QLMXNHNQpClMlI7u3vpAsphUsDRxyvYPzisohIipdBdrnV0fGKgesoaJT1UL1psnJ3X5cXllR68jtzc3mmnUrXfPzw+PePl9c1zTul90Hj/wOfXt9ByhKOjVvr5AygUPCF2SbkFAAAAAElFTkSuQmCC" />
						</svg> <a href="mailto:gerkules@gmail.com">GERKULES@GMAIl.COM</a></li>
				</ul>
			</div>

			<div class="col-xs-3">
				<ul>
					<li>Принимаем к оплате:</li>
					<li><svg
								xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink"
								width="71px" height="22px">
							<image  x="0px" y="0px" width="71px" height="22px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEcAAAAWCAYAAACSYoFNAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4gQBFw8YV/ng4AAADxdJREFUWMNtmHuQXFWdxz+/c27ffs47M5MZ8k6GRJJIQggBQoSIrAuiAiKw7rqWqFiUAruy6m5Zuqts+azdrfUBloqLu+pSgg+ISCgplw2SkEBCQt7vdzKZR+bV09Pd957z2z9ud2aCe6tu9e3uc879ne/5/n7n+z3yPxt2bXpjd3/nnv3nNEhZURW884DgvRdU8F5BVdSD4gFQBSOgCKiiCCIqiIBXFFCvgirU2ogRBAE8ogIYnHcEoXLXnUvs/gP9XxgZKz9R1oBb5TQzsgHlg/soD4/h+/p60tNnr9YTR6/xfadneyvW2nBMZ87ZbWbP3ehLY69LLtefnj2Hxqu6yXWcwTnFlwcarE1fFo0cWRqPnl4YD51sCpq6R6XQfiDVOOtNH/k9Nts0GhS6MOElmHQek8oDStDV3bLg0Sd2TXv+2QOE+QzegzpBvaIqqAdN8CCuKIpiLagXEJ/MXQUQTKAAeKcYI7hIqZYSoE3oCVIG7xPwRQyqiq/G5FqEv7h7FflsuWlgUJmT97Sq4sVAQ2MPr258IBzou8uuf7rTJDgjAijotg23O0K4cuWWuHvGE2bpssfF2KqkGm3Ut/Gv3PC+v/SDx9dKHAV4sFbQ3mFUoZIKnDTPesm3zPtZ2Pq2/0SMg9paeggqlbgqahBrCazF4/HegRHUC14UlYQ9qWASHBAUmzBCAFGMCM4riEHVIxZswaAKgTUkBPSYjAEFk1KqEYShIY4cVRe5Kp55mQlagpDhw6c+4H/z5Dez+3bOEyFhY7JOFwiJQiBVdOsrVwX7G6+aGC1dn/7b275nquvvrx568x7jnRhbIzCgsdbnD1Fs/bnDN+rg8RuLpcGbc4s++rDJNJ+szykw1oqqQb3gHKgKXg3e19JGFechjjyoYhCcF4yhliogJmGbQ/AKogBK7MB7j/dCNQJjawyLARxGDJUxSKWTmVYimNdRYMXiNgafe/4j/off/UEw2B9SB0X4f686YFIcxW5/7u7hr26+vekjDWGYt2gkICAk2SB1ZrgaSNaAd7hj2z5YHB+Y17j6K3faxtnH1FUxALF6fBzj1aPq8OpAHM5X8TiUCGMc3nk8HiRGiVGJwfikDlkPxqHqcD5C8ZjAYwMIs55U6LGBkgqVIO2wacUEnrAAGPAo6cDK8kXTqZ48cX318cf+LTifAFNnyEVI1J+nAObzWfzlbUi7hhM7KsSVCyuF6sXMEyO11NQEtEDwvcdXjL72jR9HI4enueIJgpQV9/CDq7j9trcRppKVVXUYI6SDgCBlqJZjymWHKoShJZOxlCuOMGVBhYlylXQmIB0GF4pxHHvKExGqSiZtCdOWOFac84iBKPIXQrXWcPnSTs4eGyrPGD7RNPq9734lONfb8ifUmMKe2rwufCohuiBD0BlAVXFnIiodAbn5IRp7mNrWJ1/E1AdLPiUU3Mm9a0sNP/tUuvu6LwdGYPUVs1h9xcVUPXN2kDfe7CcuRizsaeWyRV0A9J4b5ZVNJ1i5oosjR4aZGI+4+/2L6e8b46lf7+XgsWE6W9KsvX4uq959KSCcOX2ezVtOsWBBG83NIVYM3TPagEmAYudJFzLFMy88e2PzH196x0XBCH/CGJUptQdwLSnIGNzpajKsUypvKGFXCps1gCIe1PsEDNUktQQMUktbwYgSndl6X9i9+qcBSWZRjV3yUlWMUTLZNE+v38fPf7CNez+1ise++R4qlZhPPPBbDp8c4gufvY777/8tS5d10dKe5YGHX+Bc3zjLV3TxvxuG2LDtJL94/C6sMXzsb57jD787yK13XMZtt86moZDm5mkFFPA+KeTeWJozPp/dveWWi/CQ2vNUgN4ClgJ6SYhZmKuxpFZbYoiHPam8xUdJXomR2mYzOUx9bKmxxxdHu93okfcGYhJw1Nd1SAJQa3OBVStm8lR2B65WvH7xq11s+ONRfv5fd7Jt6xnG+iLec3MPTz+7j51be3nwc6v51j/eSN9Iif6+MQr5ND98YjOvv3GGfGuBctWzds1CMhmbBKUgNUoTWNKDp3vsrp3Xq4KvgYImRJAp6XTRg4IvZDF5wZwr4ssek7KoKlp1VA840h2NaJhJXjhRThhjkgL91qyt/UR07vUbjdZ5acxkoFqPN0HYxZ7+wSL//K2XufV9l3HTO+bw7PpDtM3Msuaa2YQZA0ZY98x+Hv/JNjqasizu6eTo0UG+/x9buO39C5kxM8fIyDjNjTlaGnNJ7qsiIsltBXPm5CIz0N/prltD49NPo/fcQ7zqKpqeX4fe+1HcgvnEly7AZ9LEra24lVdRXbCA7Le/T3j3B3DTGrA3XY5rzaBdDbBsJq69ndTyT9N0y1Pk1n4HbZsOhSYkFUBg0VwOzaTRMIWGOdJX3kdq7nL88PGFQb0m2XpREnMByUIhxAQB6pVv/fsrRFXHl//hBnbuGeDg/gF6Fk7j0nktzL/vag4cOc/vnzvCQ3+3njO9o3zxc+/guz/aRENzji995jru/OtfMFqMGC9VSafzGJOITGMM3iXaS4cG2gWC4MpraLvjA1TPDeJWXUf7n9/K2LY9BEuX0Xj1KsY2v0p6zlxMvonhdc/QvGYN48dmEcy5mVTbNMqd+zHGkHn7SkpHeiksuJThDQ/ix/oI599M2LEaXx3AjxwkPfd2KgPbsWErEqRJtSymuHErGkV5UyeO95pQcUqRa8ilCfMptrx+lp8+uZsHHryaBbNb+NW6/VTGI664opvO9ga62xv46WO38/Dnr8UGll//7hAvbzrCL3+zl6H+Co98/RV6e0tUI09pwqHq8d4jIhfYkxDWgUAwYzZjh05SWL2G9OJljB4+jhvsQ8eKiAQUrl1LfskVlA/sxR09QtTXR+XIcVqvXUPl0DGar/8zmm96H+U3niTX5ojOPo87sgtGB9BoFB+XCDtWk557F278BDbTBtEA1VPPg5vADR9NkmkyMl9jzqRwKORTiIFDu/pY8vYOHvj4SkZHy7zw+8MEqZAb1szie49t4sn/3sFgb4msWFxUpaUlxdf+dSP5hgzvv20R2aYU1gac7x+nWKxgTSIZpia7AKalbVAzjXG6Zx6l13fSsHA+8elTxOeHmPGlL2E7OojHxint2s7Jez9Mw+rrmPmDH6FG8MURqkOjTGzczNlHf4iYgNJrLxOfP4addgWmYxqpxXeQW3gvvnQUMRabm07U+zLWWlzxIBqX8JUxfHkCwnQluBBcLfenBtzYkKGjM0dXR45Hvng9gbHsOXSG8dEyly9vZ9mSTh55Zh8v/uEwNm2xGG665VJWrezk8Se288gX38U9ty2hVKow2D/Cpk2nKZYqWCsQkQi0ui/1wPSZ++Ku7svLJ0/PrO7aR58fZ3zDS4goE3v2keqajisVUZum+f5PUdq9l/JzL9B4+VIqe3Yw2pBi2n13Uzp0grEdG6A6zMTOH8MNy2lY+yiV83vx5SFswzyqQ/uwYZZq72toZYTMwk+iPqLatwXffQ2plD8hhw6fOzl/XseMuAaImVK9yxEcPjZMJhuwYEay9Z4fcRw/NUyhENIzu4HzIzF7Dw8ycH6CzrYcy5d0MDJaoW9wnEXzW7EWnIPTveOMFCvMndFCIS+T5rF2OYUXf/vaJ+Z8++/XBjv2fIg4QsM0EpXRIERKE2h7O4yPAx5paoahIXSgDzqm41uKyII0km2EuIKKgfEymRUZsosbQfL4iVFsugDG4KMyYgKIx1GnmEwj3sXgYnyQIbPwhq8GAJWB1ygPnQAsRhSvDtEYMcql6RQaecb3RTjnyRphcdagVU9xV0TeWlY2W2gTRMEdrNKgniYrlHdGxN5hBDpCYbo1RIciRpzHaL22JZYlO/8WhibSxY6FS37T8uIfPnRBhNTdNyDFkUlx0nduMh/7e4k1g/gMUhlJjlO8om1pwtlZpBLh46HEKxaHQQRBa5qnpgmLw8m4XrFhNc51L3ouUGJKO/6Fib2vYbKJPpC6g60zXzUxbvUoa78jUHJTCmqdCb6eKrV+qkzIpHy/YARrhsdHSirfSrW4tND9vnf+srRr62bd8MoqkYvF4FQ9MlXnIGBHleh4jJ0fQuTxVQgXhwQ5AQfG1nRVMLkJqAhSo7DU/tdYCeZc9SPbtORVk/xpMEENRZtoDgkEY5NOEhjEJq5aau2wAlaQ0KCBoIFgAklWIwBq/UgJkkr6ScpgwuQmZZCUYLOGIGMQAqpVlx5snzfU8tlP/JOb3lm8MPe3uvH/x05QqWB7I3zRI1mDnZcm97YMxJM2QUhOCaQm9qSGurpklxanmKZCr5929bGokppj1CfFWAHxtQ5al9M1I+q1ZtgSZE2dKa4myQWMkQtx1pdaplBlcruuGT/v8Sq8edRyqDdx7JGDN7efJ9MzZ7395Ec+F7e2Ourp91YwprCmftuhcfSVAZyF3LI0ZootqDc3kgjWi/A2CUA+E47nVn7800HTDMWPzQlA8NUJtKI4X9/OaylR273qKrp+JuItF/LTk7Tz1HSL1wtnqHWPVz9cNUDyiqSP856z/Y7mTAV1VTJpw7GzMdu3jbBgXtdjxc98cjT6yVNfMwcOzbQXi/eLiVQzoj6TRdauXVe4Y+bWIP3qQ3FfscWEpm4fp5gpJjUW4CseaWnqT/e86yGTa/ulwYqIIUiFQT4z/yZMphVJ2SlurzaiJuZGvdYQtzXLb5JiWmNebVlq4JqEcaq141BTK3zJmJK2oBa8593LQXCkOpaDrwYpUY4MZZgWxxRmdf2s8J2vbx97et1D1fUvftCeOt1s6yyY4spjgKuv3KUzZvyk5b4PPyq6u5Sb+7GXysc2fD46tfNmqSZHMPVFrjNePWg68KmeFc9k5t/0jbh4ZjOuCsYnp+LPv7jjV9MvaW8fHy/HxlpNDpFFxUhy7Ok8NrAU8rlwYqJS9ahmM9kgdrE3IkaMwcUuymQzGVDxilYrcTmwQShGUqCoSlmdK4sg3imnTgxppRppR0ejtE5L4xF8VAk2bNj9nXKluq7sQ97pdzEzX6aw+gaiwUH8yPnLxt/cuaa8aevV0j88S4Igi/ej0jNnj+nq2OCq8Rbb0nImu6gHWzhEy4qVVMfO5XXi9LXx0In3Rv0HVvmJYhexS0kQlCWdPZvqXrrFFi55xua6N9r89Er57BbC1h4i00o618z/AX+ww43I94TFAAAAAElFTkSuQmCC" />
						</svg></li>
					<li>Поделиться в соцсетях:</li>
					<li><svg
								xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink"
								width="90px" height="20px">
							<image  x="0px" y="0px" width="90px" height="20px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAAAUCAYAAAAN+ioeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4gQBFw8pBifg2gAAFZlJREFUWMN1mXd01GW6xz+/MiVlMpOZSSYVEhKSkBAJvQiIuipYL7IKeO2i3l2KDddyXQVFxV3bHhd3FdeKiNKLBJQESAKkGgJBCQklCaSQPpMy5VfuHzPEu7v3vufMOTNn5nx/z3zf532e7/N9hfy9+RgMBioqyunq6qajs4P4uDhbZWXl7R2dnbe2tbWNa29vtxiNRtXhcLTGx8Udc8bEfBrnctU4HA4mTJhIZmYGkiSh6zqCIACg6zroOpIk4fZ4qCgvp8/tQVEVVEVJ27lr50Oapl/T1to2wtPvMTidzsHYmJjTEZGRB8JMps3JycmXZs6chdVmw+F0omkaAKIoYrc7kCQJAFmWKT92lM6uThqbmkhKSrbu3LnjVrfHc1tfb29ea1tbFKClpKRcjnE6j4SFh/8j2mY7PmXqVBLi4klLS8MVF4eiKIiiyMDgEH19vQiCiAAoqkpbSws9Pd2UV5Rx9uy5ZK/X+8jA4OD1LS0tI/r7+w0Oh2MoxumsMxgNP86dO2+rQZKbDEYDVksU06ZPRxRFZCHICgBhZjMXm5vv3r9//9q2trZUgKzMTMaMGcPQ0BAXL15M+qm6ejLw+IQJEzZMHD/+ZZPJ1IIggCAgXCH4yhIErnySZRmjQTYXHix8sba2dnlfX5/N6XQyenQ6VpuNjo4Ozpw5k+b3+28B/nvWzJlvmEym90RRGIa7gi0Iwj9vKhAeHkFTY+OCnbt2vdXS0pIGkJmRQWZWFl6vl4vNzYkVlZXjgceys7M3js3N/aPJbL5ICEMIxSpcAURHEEXQdQyyjCBgOnvu3B+OlZauAJwOh4OMjAysViudnZ2cPXt+1OBg/7zm5ot/zBs37o2bb77l7Sv/X9d1ZAQBQRQJM4dRePDg0wWFhX92OBzic889x913L2T06HQsFgsBRaG3t5djx46y9s21pmPHjj3S3Nw8JzNrzD1jsseUq4qCruvDZGiqOkyQyWCgr68vafPWrRtra2tnZWdns27dOq65Zg4xMU5MJhMDA4NcvNjMrl27WL9+vbO4pOTdiIiI9IceeniFKEmqqqpB/BAdV0gGCAsPZ//+fcu/z89/z263SytXrmTRokVkZGRgsVhQVJXe3l7KSktZu3atsaSk5MG+vr5rH3zgwXszMjJK9BC3wq8cIwgCmqZhMpno7u6K/8dnn208fvz4nIyMDJ577nluuulGHA4HZrP5ShKye/ce/v73D6MPHT78Z03TMu6cP3+pJEkBXddhz67dHCw8yP333f8EoM+YMUM/deqUfmX5/QHdHwjofr9fV1RV13Vd9/l8+prXXtMB3eVytW3bsjW+pLiEwoICCg4c4GjJEaqrqqiqrORETQ0/7N9vT0tL+wnQH374Yb2jo0PXdV1XNU33+Xx6IBDQfX7/8DM7Ojr0BQsW6IA+96a5H+3L38fBwoMUHCiksOAglZU/UV19nOrqGn75pY7f/e73vwf0KVOm6LW1/yv2QED3+Xy63+/XA4oyHPubb67VAd1ut3dt2bJ1zIXzFzj9yy+cPn2a49U1FB0upriohNJjZezdm2/OysoqA/T7779fb29vH8YPBAL6v66Ojk594cK7dUC/9tprPz9acoSSkiNICxcuoqS4eMYH6/66MTU1Vdq2bRuZmZkAeL1eTCYjqqoS8PtBgIA/gNlsZvbs2STEJ7Dp202RjU1NtrE52bv6+tx4PG48nn7c7j56enrw+X18+eVXKw8XFd3z5JNP8uGHHxIeHo7XO4SmaQiiAAIYDQa8Xi+KohAVFcXcuXM5cuQohYUFEy0WS9u4q66q8nq9KIEAzc1NdLS343b3UVxcNO3999/blJiYKO3YsYOsrCxUTUMUBCRRRJIkJElCFEU0PVgOZs+axYgRI/j222/D2lpbXVOnTNnc3d2Np8/NoHcw2Ec0FaPJxKZN3/zX/v37H126dCnr168nIiIcn8+Ppmv4/X42b97MoUMH8fn8xMTGYLVauemmuZSWlnLw4ME8RVV7pkyZXCaUFB+RH/+vx4obGhqmbd++jXnzbqa3t5dPPvmE9rY2KiorycnJ5pFHlnDVuHGIgsB7777LfQ88QGxMDMuWLmPdh+v0Na++Nicvb1yRoihcuHCBxqYmDAYDQ0ND8Z9/8UXNpImTYn74cT+yLDPk9Qabp6YjyzKdXV384dlnMZlMfPDXD5BlA5Io0tDQwJw5c/B6vR3Lly3LjYiIbA8EArS2XERTNWRZFnfu3l14obHxmp07dnL7HbejaRr19fW8+eabaJqGpqrIsoGk5CTuuutucnKyEQSQJJmnn36a9957j8ceffSG0emjD/gDfkRBRBQFBFFicHDA9f7775/Izb0q9kDBAQxGAz6fH1EIlq6Fd9/N9h07AHA6nZw6dQq73Y4syzQ2NjJr1iwG+vt7XluzJleKslh+s3vP7hd+u2ABL7zwIl6vl/nz52O12njhxRc4duwYe/bs5plnnsVqjUIQBD799FNmXH01NquVcXl5fPXVV0JTU6Ml2mbb3NjUjB56sMPuoLyi4onaU7W3bdiwgZSUFIa8XkRRxGgwIssyAP95zz1s3rwZTdMwm81kZ2cjyRIOhwOfz8uePXsikpKTO2ddPfNIWHg4uqZjs0fT0dE5p6Cw8JX58+fz8isvEwj4kWWZs2cbWL58OSdOnODkyZPU1NRQXFzM4cOHePDBB5FkA6IgMHnSJL7euJGenh777bff8Y3FEkWUNYpIi4VYl4uiosPLKquq7vj8889JT0/H5/UhCAImk4lTp07xxJNPEhMTw6pVq5h9zTXMnj0bgIASwOlwomk6O3ftDPP7fH3iqV9+vgtgyZIlAJw+fZoDBw5w8y03Ex0dzeLFi2lvv0xrSws7d+zk0SVLqKqq4o8vvcTKlSuJjo4mJyeHEydPXn/x0iVXb28P7r4++vv76enplqqPV8+fNm0q06dPx+/3I4aO88mTJ6irO01+fj67d+8G4Pjx4yxZsoR33nmX7q5uvD4v9953P06nk4ryipvbL1+mu6cHfyCAqmmcrqu7E2DJI8HYtZDECeofSE1NpaG+gR07dhAeHk5jYxMtLa3IkoTP78fhdDJ79mx+rvllzqWWS4kBRSEQUNBVFe/ggFBVVfUfaaNGMXXaNAJKAEEUEUSRzs5O8vPzAXDY7cyYMZ158+aFYtAQBAFVVVm8eDEul4uGhoYbxfr6+kkJCQlMnDQJgPDwcAC+2bgRgIGBAZwOB7EuF3OuncPLq1aTPWYMy5ev4OmnnyY8PIzJkycDRIeFh48de9VVxMTGYjKZ8PsDCS0tLZlZWWOCNVJTg0dTEHnzzbXccMONvLp6Nf+6PvxwHa+//jqaqpGclER2TjbdPd3xAhjDzWZkWUIEmi82j3O5XEyeMhk1pLOH1VlIspnM5mHNPWrUKFwuV7A3hH6VmZFJAH9kW2vbOLPJiCRJGIxG+gcGXE3NzWNSUlMIDwtDUzXQdYwGA3v37uX5558PJmZdHTNnzuJPb72F0WhEDymWQMBPQkI848aNo6u7O06+cOFC7PTp04mIiERRVdLS03nllVfYtm0bTz75JOfPnWfDxo0kJycRCDWqzKwsEhMTSUiID2VOCgB1dXVpXu9QwcWLl7h8uR1N0+OBSJvNBoAky0iSzI7t2ykoKKCjo4Pm5uZ/I9rtdmO1WpHkIEFpo9IoOlwU+e1335oF8Pe6+wCE9vb2qKzMTCxRUWiaOkyeIAgYjUbOnTtH5uh0Br1eAKZPn054eDiKoiCIQezEpEQACgoLRp2oOT48GAUC/kSfz2dNSkoODUoCqqah6ToJCQnk5uZy8uRJoqKiyM3N5dZbb0PTNUQxKAslKVgWR6Wm8sMPP0SIAJIkYTQauKKFV61aRXV1NYIgcPDQQZISkwBQFYWAovDMM8/gdDoZGhoKEigZAOj3eOTuru5Q6RhgYGBABJAN8vCR1lSVjz/6iI6ODv6/NW3aNFatWjWcmqGMFNxut+D2eOj39NPv8QgDAwOCKEnIkgT68NyFpqn4/X4SE5P4aP0nfPnll0yZPIX169fz+uuvYzKZhnW+0WgEYGhwUHJ73LjdbtweDx5PvwhgCPURRVERQ5n6m9/8htWrXwXAbo9mz549LFhwJ4FAAFVREUUJRVGGkwsQ5Pj4eE9rSwt9fW4slkh8fj8frltH/Zl6Yl2xjB2by3XXXUt+fj55eXn4/b4rZQFDiMDW1hYA8saPvzh2bC5dnZ0MDg4wMDDQdbquzt/b02sE6Orqxmw289WGDZSVlfPcc3/g559//jeib7vt1uDGiCIALS0t2KOjh+6cP98nSRIdnZ0IoPUPbBq6fPkybrcHq9U6nI1XmqzFEsG99/4nAE2NTZRXlFNZWRHK0CD2pUuXAJg9e3ZL7tgcvD4/oijh8bg7yysrh1rb2sIAZKMBNaAMD0q+0CnRNB1N11BUBQGQZAlNU5ENhmH8sLAIr5yUmFhTUVmZefZsAxMmTOD1NWvYumUr23dsJysri5deeolFixaxZs3rbNu2FVGSUVQFWZZQVRWDwUB5eTmA1+f11tad/gWTyUxkZARGo7HJbo++0NjYmHFlQ/77xRcZlZaGw+Ggq6vr30ieNXMmDz/8CKqqIooiPT29nDx5EktUVGeU1epVFQVLZCQGg4H4uLj6isrKqfX1Z5g6dSpKaBhVQm96enp5dfWrePr7h3vOtGnTgwSF0v/cuXMAysiRI2siLVZM5qB8i4qyXExMSDx3/vz5HEVR0HUNURLRQx1XlMThMqUqwd6joqJrGqIY9Ek8nn5qamqw2aI6RafTuRtg06ZNAFRVVnL1zKvJysoaBsoYncGw56DrCAS9BoPBgMfjobb2FCNHjCi32+3nAUwmI+Hh4TicDt/YnLE/VlVVce7cOfLy8nj1tVepra1l9erVtLe3DxNssVhYvHgx3373HZGWSBRFQZYk8vd+T3NzM3l5eUdGjkzBGevCYDQiGwxkZWbuAdi48ZsQivBPm9be3s4rq17h7bf/TGdXJ7fccgtPPPEE/kAASRTx+/2UlpaSmpr6U0pKyllREAgPDyci0oIr1hWYOHHCD83NzTQ0NGCQDUF7IfQITf21+QqiELIGfvVhZFlm3759nD9/nozRo8vl5cuX76qtPdX4xRdfjFyxYgULfnsXzz67kqSkJKZNm8bJEyfZtn0rn33+RXAA0DUkUURRNSRJ4rNPP6OpqZFly5Z9NHHiJN0fCNDe1kZbWxtGo5HMzMyPioqLl7zxxhumTz75hIkTJ1FQWMihQ4eoKCvHM9AfkkhXM3XqFHRdx+fzYZCDm/jWn/4E4M9IT/+k/kwdAX8Ak9GEIIqMzxv/feGhQ2c3fv112mOPPUpOTg4+v5+MjAwKCgqHk0PXITraRnZOzrC0CzOb+fjjj6mtrWXhXXf/vfXSJdXr84VKg4DBIJMxevQ/du3e/fs33njD9OWXXyKESgUEtTLA4OBgUE7qQfNIFEQkScTr9fLWW2sBAkmJiR9JDz30kG9oaLCrpKRk/k9VVfzp7T8zZfJkSktLqampQTbIvP32O4wdOzaog4WgTAszh1FSUszvfr+UuDjX4UV3L3xeVVVNVVUMRiMWSxRRUVGMGpV2ube3N3X79u3jR4wYwfjx4wkEAqSnpzNz5kyuu+46pk+fTlJSIoqq4vf5kCUDsiyzdOlS9u3bx6233PrXO+6Y/5VsNBEdbScrK5Pk5CRGZ6T7NVXrKCgsXFBdXc3ChQsJDwtDEARGjx5NSkoKKSkppKamEBfnQgf8fj9hYWGUlZfz2KOPkpCQUL5s2bKnIyIjVavNhtVmw2aPJioqiozMzA632x2zZcuWqXFx8UydOjUoDQWBvj437e3t5ObmMnfeXMxhYWhaUHXIsoHly5eze/du5lxzzcc33nDDp2zbspW9e77n+uuu+xTQH3nkkX8zShRF0X0+n+71eXVV03Rd1/X8/Hw9OTlZB9R333lnwt7v97J923a2bd3GgR8LqKiooqysguPHT7Bx46ZMu93ebTab9XXr1g3j+nw+3ef364ODg3og4Ne9Xt/wd6tWrdIBPTs7++jmzZsjjx0rpehwEUWHiyguKqL4cBFHioqpKCvj5nnzPiZk+gwNDQ1j+/0+fWhoSPd6vUFTLGQs/fjjAX3EiBE6oL/y8ss3lJeVc+jgIQ4dOkTx4SKOHj3KsaPHqKyo5JuvN7piYmIuA/pf/vKX/9NQ8gf8us/3a+yvr1mjA/qYrDGV+/bmW8tKS5Eef+xxwsLCiIyM2Nfa2jpq3759uT8eOEBsTCxJSYkYjcbhaU6WZJqbm3n5jy+zYsUK3G63f/my5UsXLVq0x2KxYLPZsNvtREREIIgiYuhls9q6NFWtaDhbf/PmzZsj6uvryc3NJTY2NjggGAyIYrBbl5aW8tRTT/G3v/2N7DHZpU899dSCkSOSuvv7PQT8fhRVwRplxWgwYDAaCQsLw2a17WtpbRmZn58/7sCBApxOByNTUgkzhyHLMrIsI0kSLS0trF61mpUrn6Gzs1N94IEHVtx6y+2bhoaGUAIKSkBFVRSUQIBAIIDP58Nmsw3YHfayurq6eVu2bImsra1lzJgxxMfHD9doSQxeepSWlrLymZV88NcPSE9Pr3zgvvvunDR58mVBEBAO7P8BWZYpKy+jr69POFxc/PaRI0eeAoTMzEwmTpxIamoqbrebsw0NVP30E+3t7SQnJ5+YMnnyM4sX33Ng5MiRw3JJ13UEURqWQaIo4na7OV79E83NTWO2bt36l3Pnz99gs9m46qqryM7OxuFwcunSRWpqaqiurgYgJyfn28cffeyxWJfL7XDYg7o3dLlgtzuG8WVZpry8HLfbzXebv1tbVl7+LCBmZGQwceJEUlJSGBwc5MyZM1RXV9PW1kZcXNzJO26//Q/XXXf9PpPJTHh4+PCNkCgKiKIYrLeiiKIodHV301Bfn7nh6w3v1505M9disTB+/HiysrJwxsTQ1tpCTc0JqqqqAMjIyNhy3733LYmKjOybPmMGoiyHiDYYKC09RkdnBy5XHE1NTTMOHT70YEdH56T29vYYwARoQH9ycnJzYkLChlGj0jbFxbkG8saPJyszC0mW0YevmyT4F6LLy0rxuN14+j309vb+du/evUu8Pl9qb29vFCABAYfD0RVlsZy+8cabvnLFxOyOdjiIcTpxxbl+JVoQsEfb/4no0tJSuru7uNDYiNlsnnbo8OGH2tvbJ7W1tcUAZkCLjIwcjI6ObopxOjfGx8dvnHH11f1JSclYo6yEhYUNTzvBUygQmqVRFYWWtja6e7rpuHyZsvLy286cOfP4wMBAek9Pj/VK7Pbo6O5Ii+X09ddfv8EeHb3TYonCEhnJrNmzEUWR/wFeOKga8wPjlQAAAABJRU5ErkJggg==" />
						</svg></li>
				</ul>
			</div>

		</div>
		<div class="row">
			<div class="col-xs-12">
				<p style="color: white!important; text-decoration: unset; font-size: 18px; font-weight: lighter;">Все права защищены магазин real-3d.ru &copy; 2018</p>
			</div>

		</div>
	</div>

	<div class="container" >
		<div class="row">

			<?php if ($footer_top) { ?>
			<div class="container">
				<div class="footer_modules">
					<?php echo $footer_top; ?>
				</div>
			</div>
			<?php } ?>			
			<?php if ($footer_accordion) { ?>
			<div class="accordion-footer container">
				<?php echo $footer_accordion; ?>
			</div>
			<?php } ?>
		</div>
	</div>	
	<!--<div class="copyright">
		<div class="container"> <?php echo $powered; ?><!-- [[%FOOTER_LINK]] --> <!--</div>
	</div>-->
</footer>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<button type="button" class="close best_price_warranty_button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			<!--<div class="modal-header" style="border: none;">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">-->

				<div class="well">
					<h2>Вход кабинет</h2>
					<form action="/index.php?route=account/login" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label class="control-label" for="input-email">E-Mail</label>
							<input type="text" name="email" value="" placeholder="E-Mail" id="input-email" class="form-control">
						</div>
						<div class="form-group">
							<label class="control-label" for="input-password">Пароль</label>
							<input type="password" name="password" value="" placeholder="Пароль" id="input-password" class="form-control">
							<a href="/index.php?route=account/forgotten">Забыли пароль?</a>
							<a href="/index.php?route=account/register" class="pull-right">Регистрация</a></div>
						<input type="submit" value="Войти" class="btn btn-primary">
						<input type="hidden" name="redirect" value="http://real-3d.loc/index.php?route=account/account">
					</form>
				</div>
			</div>

		<!--</div>-->
	</div>
</div>
<!-- Modal -->

<div class="ajax-overlay"></div>
<div class="ajax-quickview-overlay">
	<span class="ajax-quickview-overlay__preloader"></span>
</div>
</div>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/device.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/livesearch.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/common.js" type="text/javascript"></script>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/script.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        var cart = $('#cart-total2').text();
        var another_count = $('#cart-top-count');
        if(cart != '0') {
            another_count.html(cart);

        } else {
            another_count.html(0);
        }

        $('.curs').attr('href', '');
    });

    $('.curs').attr('href', '');
</script>
<!-- coding by xena -->
</body></html>