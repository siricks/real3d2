<?php
class ModelExtensionPromotion extends Model {
    public function getNews($news_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON n.news_id = nd.news_id WHERE n.news_id = '" . (int)$news_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getAll($data) {
        $sql = "SELECT * FROM " . DB_PREFIX . "discount_promotions ORDER BY id DESC";

        if (isset($data['start']) && isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 10;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotal() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "discount_promotions");

        return $query->row['total'];
    }
}